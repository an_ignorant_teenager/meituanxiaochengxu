//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    listitem:[
      {
        title:"美食",
        icon:"icon-wodemeishi0101"
      },
       {
         title: "猫眼电影",
         icon: "icon-maomi"
      },
      {
        title: "酒店住宿",
        icon: "icon-jiudianzhusu"
      },
      {
        title: "休闲娱乐",
        icon: "icon-xiuxiankafeiyule"
      },
      {
        title: "外卖",
        icon: "icon-_waimai"
      },
      {
        title: "KTV",
        icon: "icon-maikefeng"
      },
      {
        title: "丽人/美发",
        icon: "icon-nvren"
      },
      {
        title: "景点门票",
        icon: "icon-jingdian"
      },
      {
        title: "火车票",
        icon: "icon-huoche"
      },
      {
        title: "摩拜单车",
        icon: "icon-zihangche"
      },
    ]
  },
  //事件处理函数
  bindViewTap: function() {
   
    
  },
  onLoad: function () {
   /*
    wx.showModal({
      title: '定位到您的位置在A城市',
      content: '是否切换到A城市',
      success(res) {
        if (res.confirm) {
          console.log('用户点击确定')
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
    */
  },
  //点击右上角转发
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '美团',
      path: '/pages/index/index'
    }
  }

})
