// pages/order/order.js
Page({


  /**
   * 页面的初始数据
   */
  data: {
    switchtab: [{
      name: '未使用',
      _type: 'notUsed'
    }, {
      name: '已使用',
      _type: 'alreadyUsed'
    },
    {
      name: '已过期',
      _type: 'expired'
    }
    ],
    currentTab: 0,
    coupons: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      coupons: this.loadCoupons()
    });
  },

  //tab切换函数，让swiper当前滑块的current的index与tab头部index一一对应
  switchNav: function (e) {
    var index = e.target.dataset.current;
    if (this.data.currentTab == index) {
      return false;
    } else {
      this.setData({
        currentTab: index
      });
    }
  },
  //滑动swiper切换，让swiper当前滑块的current的index与tab头部index一一对应
  tabChange(e) {
    this.setData({
      currentTab: e.detail.current
    })
  },
  //自定义数据函数
  loadCoupons: function () {
    let switchtab = this.data.switchtab,
      coupons = [{
        id: "1",
        price: "200",
        condition: "无门槛",
        goods: "新用户 20元优惠券；",
        way: "限无人机、数码、潮玩",
        date: "2019.3.22-2019.12.22",
        _type: "notUsed"
      },{
        id: "1",
        price: "100",
        condition: "无门槛",
        goods: "新用户 10元优惠券",
        way: "限无人机、潮玩；",
        date: "2019.7.22-2019.9.22",
        _type: "notUsed"
      }, {
        id: "2",
        price: "200",
        condition: "无门槛",
        goods: "新用户 20元优惠券",
        way: "限无人机、数码、潮玩；",
        date: "2019.3.22-2019.12.22",
        _type: "alreadyUsed"
      }, {
        id: "2",
        price: "100",
        condition: "满500可用",
        goods: "仅可购买网络品类指定商品",
        way: "全平台",
        date: "2019.5.12-2019.12.12",
        _type: "alreadyUsed",
      }, {
        id: "2",
        price: "100",
        condition: "满500可用",
        goods: "仅可购买网络品类指定商品",
        way: "全平台",
        date: "2019.6.0-2017.12.0",
        _type: "alreadyUsed",
      }, {
        id: "3",
        price: "200",
        condition: "满1000可用",
        goods: "仅可购买网络品类指定商品",
        way: "全平台",
        date: "2019.3.1-2017.5.1",
        _type: "expired"
      }];
    //这里是根据tab头部的数据来重建一个数组，使数组的内容与tab一一对应
    var result = new Array();
    for (var n = 0; n < switchtab.length; n++) {
      let minArr = []
      for (var i = 0; i < coupons.length; i++) {
        //根据类型来区分自定义的内容属于哪个tab下面的
        if (coupons[i]._type == switchtab[n]._type) {
          minArr.push(coupons[i]);
        }
      }
      result.push(minArr)
    }
    return result;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})